

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dataProcessing.BuildLocation;
import dataProcessing.DataCruncher;

/**
 * Servlet implementation class DataManagerServlet
 */
@WebServlet("/dataManager")
public class DataManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataManagerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		System.out.println("Will attempt to use the data cruncher");
		float budget = Float.parseFloat(request.getParameter("budget"));
		float size = Float.parseFloat(request.getParameter("size"));
		String gender =  request.getParameter("gender");
		String yearLevels = request.getParameter("yearLevels");
		int schoolCapacity = Integer.parseInt(request.getParameter("capacity"));
		float tuitionFee =  Float.parseFloat(request.getParameter("tuitionFee"));
		int priority = Integer.parseInt(request.getParameter("priority"));
		
		System.out.println("budget "+budget);
		System.out.println("size "+size);
		System.out.println("gender "+gender);
		System.out.println("Year Levels "+yearLevels);
		System.out.println("tuitionFeee "+tuitionFee);
		System.out.println("Priority "+priority);
		
		DataCruncher dc= new DataCruncher();
		dc=  new DataCruncher(budget, schoolCapacity, size, priority, gender, yearLevels, tuitionFee);
		   
		//BuildLocation tempBuild =dc.selectTopPicks(3);
		ArrayList<BuildLocation> multiLocation = dc.selectTopPicksMulti(5);
//		for(int i=0;i<5;i++)
//		{
//		multiLocation.get(i).setMetadata(dc.generateMetadata(multiLocation.get(i)));
//		}
		session.setAttribute("multiLocation",multiLocation);
		//session.setAttribute("bestMetadata", dc.generateMetadata(multiLocation.get(0)));
		request.getParameter("p");
		//System.out.println("Crime: "+ tempBuild.getCrimeLevel()+" Land Price: "+tempBuild.getLandPrice());
		//session.setAttribute("best",tempBuild);
		
		System.out.println("Supposedly finished using the data cruncher");
		//session.setAttribute("best", DataCruncher.selectTopPicks(3).getxCoord());
		//System.out.println("Session data successfully added: "+ DataCruncher.selectTopPicks(3).getxCoord());
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
