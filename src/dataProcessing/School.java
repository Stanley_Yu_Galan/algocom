package dataProcessing;

public class School extends Location {


	int numStudents;
	String type;
	String yearLevel;
	String gender;
	float annualTuition;
	
	public School(String name, int numStudents, String type, String yearLevel, String gender, float annualTuition, double xCoord, double yCoord) {
		this.name = name;
		this.numStudents = numStudents;
		this.type = type;
		this.yearLevel = yearLevel;
		this.gender = gender;
		this.annualTuition = annualTuition;
		this.xCoord=xCoord;
		this.yCoord=yCoord;
	}
	
	public School()
	{
		
	}
	
	public int getNumStudents() {
		return numStudents;
	}
	public void setNumStudents(int numStudents) {
		this.numStudents = numStudents;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getYearLevel() {
		return yearLevel;
	}
	public void setYearLevel(String yearLevel) {
		this.yearLevel = yearLevel;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public float getAnnualTuition() {
		return annualTuition;
	}
	public void setAnnualTuition(float f) {
		this.annualTuition = f;
	}

	
	
}
