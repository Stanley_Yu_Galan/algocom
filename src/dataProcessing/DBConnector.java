package dataProcessing;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;


public class DBConnector {

	
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //"Customize these to match with your database configuration";
    final static String dbName = "algocom";
    final static String DB_URL = "jdbc:mysql://localhost:3307/" + dbName;
    //String tableName = "cake";
    final static String USER = "root";
    final static String PASS = "password";
    
	public static Connection connectToDB()
    {
        try {
        	 Connection conn;
			 Class.forName("com.mysql.jdbc.Driver");
	         conn = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
	         return conn;
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//          gui.appendToOutbound("Connecting to database..." + "\n");
          
          return null;
    }
}
