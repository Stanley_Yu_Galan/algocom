package dataProcessing;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;

public class DataCruncher {

    public DataCruncher(float budget, int schoolCapacity, float requiredLand, int priority, String genders, String yearLevel, float tuitionFee)
    {
    this.budget=budget;
    this.schoolCapacity =schoolCapacity;
   	this.requiredLand =requiredLand;
   	switch(priority){
   	case 0: safetyMultiplier =0.50f;
    accessibilityMultiplier =0.30f;
  	economyMultiplier =0.20f;	System.out.println("Safety engaged");break;
   	case 1:safetyMultiplier =0.30f;
    accessibilityMultiplier =0.50f;
  	economyMultiplier =0.20f;	System.out.println("Accessibility engaged");	break;
    case 2:safetyMultiplier =0.30f;
    accessibilityMultiplier =0.20f;
  	economyMultiplier =0.50f;	System.out.println("Economy engaged");	break;
  	
  	default: safetyMultiplier =0.50f;
    accessibilityMultiplier =0.30f;
  	economyMultiplier =0.20f;	System.out.println("Something Wrong engaged");break;
	}
  	this.genders=genders;
  	this.yearLevel = yearLevel;
  	this.tuitionFee = tuitionFee;
   
   	 
    }
    public DataCruncher()
    {
    	
    }
    
	/*========================Inputted Requirements========================*/
	 float budget=0;
	 final int GRID_SIZE = 2500;
	 int schoolCapacity =50;
	 float requiredLand =500;
	 float safetyMultiplier =0.50f;
	 float accessibilityMultiplier =0.30f;
	 float economyMultiplier =0.20f;
	 float tuitionFee =0;
	 float literacyRate = 98.7f;
	 int  areaPopulation = 121430;
	 String genders ="male";
	 String yearLevel ="elementary";
	 ArrayList<BuildLocation> candidateLocationList = new ArrayList<BuildLocation>();
	
	 final String LOCATION_ID = "id";
	 final String LAND_PRICE = "landPrice";
	 final String LAND_SIZE = "landSize";
	 final String STUDENT_POPULATION = "studentAgePop";
	 final String NOISE_LEVEL = "noiseLevel";
	 final String CRIME_LEVEL = "crimeLevel";		
	 final String GEOLOGICAL_STABILITY = "geologicalStability";
	 final String ACCESSIBILITY = "accessibility";
	 final String X_Coord= "xCoord";
	 final String Y_Coord= "yCoord";
	 
	 /*=================Other locations==============*/
	 final String NAME ="name";
	/*========================Fixed For San Juan========================*/	
	 float educationPercentage =50;
	//probably going to make a constructor that grabs the specified criteria
	public  int limitScore(int score)
	  {
		  if(score > 100)
			  return 100;
		  if(score <0)
			  return 0;
		  else return score;
	  }
	
	public  float limitScore(float score)
	  {
		  if(score > 100)
			  return 100;
		  if(score <0)
			  return 0;
		  else return score;
	  }
	
	public  float limitScore300(float score)
	  {
		  if(score > 300)
			  return 300;
		  if(score <0)
			  return 0;
		  else return score;
	  }
	public  BuildLocation loadBuildLocation(ResultSet rs)
	{
		BuildLocation tempLocation =new BuildLocation();
		try {
			tempLocation.setId(rs.getInt(LOCATION_ID));
			//tempLocation.setCrimeLevel(rs.getInt(CRIME_LEVEL));
			tempLocation.setLandPrice(rs.getFloat(LAND_PRICE));
			tempLocation.setLandSize(rs.getFloat(LAND_SIZE));
			tempLocation.setStudentAgePop(rs.getInt(STUDENT_POPULATION));
			//tempLocation.setNoiseLevel(rs.getFloat(NOISE_LEVEL));
			//tempLocation.setGeologicalStability(rs.getInt(GEOLOGICAL_STABILITY));
			//tempLocation.setAccessibility(rs.getInt(ACCESSIBILITY));
			tempLocation.setxCoord(rs.getDouble(X_Coord));
			tempLocation.setyCoord(rs.getDouble(Y_Coord));
			tempLocation.setFloodLevel(rs.getInt("floodWarning"));
			tempLocation.setFaultLineDistance(rs.getString("faultLineDistance"));
			tempLocation.setBarangay(rs.getString("barangay"));
			
			System.out.println("See if actually copied coord: X: "+ tempLocation.getxCoord()+" Y: "+tempLocation.getyCoord());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tempLocation;
	}

	float findDistance(BuildLocation buildLocation, Location landmark)
	{
	  double earthRadius = 3958.75;
	  double dLat = Math.toRadians(landmark.getxCoord()-buildLocation.getxCoord());
	  double dLng = Math.toRadians(landmark.getyCoord()-buildLocation.getyCoord());
	  double a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
			  Math.cos(Math.toRadians(buildLocation.getxCoord())) * Math.cos(Math.toRadians(landmark.getxCoord())) * 
			  Math.sin(dLng/2) * Math.sin(dLng/2);
	  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	  double dist = earthRadius * c;
	  double meterConversion = 1609.00;
	  
	  System.out.println("THE differnce of the distance is: "+dist * meterConversion);
	  buildLocation.concatMetadata(String.format("%.1f", (dist * meterConversion))+" meters away from "+ landmark.getName()+"</br>");
	  return (float) (dist * meterConversion);
	}
	
	float findDistanceNoLog(BuildLocation buildLocation, Location landmark)
	{
	  double earthRadius = 3958.75;
	  double dLat = Math.toRadians(landmark.getxCoord()-buildLocation.getxCoord());
	  double dLng = Math.toRadians(landmark.getyCoord()-buildLocation.getyCoord());
	  double a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
			  Math.cos(Math.toRadians(buildLocation.getxCoord())) * Math.cos(Math.toRadians(landmark.getxCoord())) * 
			  Math.sin(dLng/2) * Math.sin(dLng/2);
	  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	  double dist = earthRadius * c;
	  double meterConversion = 1609.00;
	  
	  System.out.println("THE differnce of the distance is: "+dist * meterConversion);
	  return (float) (dist * meterConversion);
	}
	public  Location findNearest (BuildLocation origin, String type)
	{
		//Actually try to look for it
		ImportantLocation iL = null;
		int searchFor =0;
		switch(type)
		{
		case "highway":System.out.println("Searching for highway");searchFor=1; break;
		case "flood":System.out.println("Searching for Flood"); searchFor =2;break;
		case "dangerousUtility":System.out.println("Searching for dangerousUtility");searchFor =3;break;
		case "bar":System.out.println("Searching for bar");searchFor =4;break;
		case "residential":System.out.println("Searching for Residential"); searchFor =5;break;
		default: System.out.println("HUH!!!!");
		}
		Connection conn = DBConnector.connectToDB();
		String query = "SELECT * FROM otherLocation WHERE type = "+ searchFor+ " ORDER BY ABS(yCoord*xCoord-"+ (origin.xCoord*origin.yCoord)+") ASC LIMIT 1";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next())
			{
				System.out.println("Found something at X: " + rs.getDouble(X_Coord));
				System.out.println("Found something at Y: " + rs.getDouble(Y_Coord));
				System.out.println("It is a " + rs.getString(NAME));
				iL= new ImportantLocation(rs.getString(NAME), rs.getInt("type"), rs.getDouble(X_Coord), rs.getDouble(Y_Coord));
			}
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  iL;
	}

	
	public  float evaluateNoise(float noiseLevel)
	  { 
		return limitScore((70-noiseLevel)/35 * 100);
	  }
	  

	public  float evaluatePrice(float candidatePrice)
	  {
		float difference = budget-candidatePrice*requiredLand;
		float differenceScore = difference/budget;
		float score = 70 + 60*differenceScore;
		return limitScore(score);
	  }
	
	public  float evaluateFlooding(int floodLevelHistory)
	  {
		int score =100;
		score -= 33*floodLevelHistory;
		return limitScore(score);
	  }
	public  float evaluateFaultLine(String faultLine)
	  {
		int score =0;
		switch(faultLine)
		{
		case "2 - 2.99 km": System.out.println("Fault is 2 - 2.99 km away");score =60; break;
		case "3 - 3.99 km": System.out.println("Fault is 3 - 3.99 km away");score =80; break;
		case "4 - 4.99 km": System.out.println("Fault is 4 - 4.99 km away");score =100; break;
		case "> 5 km" : System.out.println("Fault is > 5 km away");score =100; break;
		default: System.out.println("WHAT IS THIS FAULT LINE DISTANCE!!!!!!");
		}

		return limitScore(score);
	  }
	  
	public  float evaluateArea(float candidateLand)
	  {
		  float difference = candidateLand-requiredLand;
		  float differenceScore = difference/requiredLand;
		  float score = 70 + 400*differenceScore;	  
		  return limitScore(score);
	  }

	public  float evaluateHighwayDistance(float distance){
//		at least 300 m peak at 800m then drops
		if(distance <300){
				return 0;
			}
		float spillOver = distance-800;
		float score =50;
		score += (distance/16);
		score -= spillOver/48;
		return limitScore(score);
		}

	public  float evaluateFaultLineDistance(float distance){
//		at least 500m away from a fault line
		float score =  (distance/10) -35;
		return limitScore(score);
		}
	public  float evaluateResidentialDistance(float distance){
//		RESIDENTIAL FIXX MEE
		float score =  (distance/10) -25;
		return limitScore(score);
		}
	
//	public  float evaluateFloodDistance(float distance){
////		FLOOOOOODDDD
//		float score =  (distance/10) -35;
//		return limitScore(score);
//		}
	public  float evaluateDangerousUtilityDistance(float distance){
//		at least 300m away from a dangerous utility
		float score =  (distance/10) -25;
		return limitScore(score);
		}
	
	
	public  float evaluateCasinoDistance(float distance){
//		at least 200m away from a dangerous utility
		float score =  (distance/10) -20;
		return limitScore(score);
		}
	public  float evaluateBarDistance(float distance){
//		at least 200m away from a dangerous utility
		float score =  (distance/10) -20;
		return limitScore(score);
		}
	
	public  float evaluateConflictOfInterest(BuildLocation candidate)
	{
		//Criteria match
		System.out.println("Evaluating conflict of interest");
		float conflictFreeScore=300;
		ArrayList<School> nearestSchools = new ArrayList<School>();
		float damageFallOff = 1;

		String query = "SELECT * FROM schoollocation ORDER BY ABS(yCoord*xCoord-"+ (candidate.xCoord*candidate.yCoord)+") ASC LIMIT 5";
		try {
			Connection conn = DBConnector.connectToDB();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next())
			{
				School tempSchool =new School();
				tempSchool.setName(rs.getString(NAME));
				tempSchool.setxCoord(rs.getDouble(X_Coord));
				tempSchool.setyCoord(rs.getDouble(Y_Coord));
				tempSchool.setAnnualTuition(rs.getFloat("annualTuition"));
				tempSchool.setGender(rs.getString("gender"));
				tempSchool.setType(rs.getString("type"));
				tempSchool.setYearLevel(rs.getString("yearLevel"));
				nearestSchools.add(tempSchool);
				//iL= new ImportantLocation(rs.getString(NAME), rs.getInt("type"), rs.getDouble(X_Coord), rs.getDouble(Y_Coord));
			}
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(School otherSchool: nearestSchools) {
			
			damageFallOff = 2500/findDistanceNoLog(candidate, otherSchool);
			System.out.println("Distance to other School is: " +findDistance(candidate, otherSchool));
			if(tuitionFee >.75*otherSchool.getAnnualTuition() && tuitionFee <1.35*otherSchool.getAnnualTuition())
				conflictFreeScore -=100* damageFallOff;
			if(genders.equals(otherSchool.getGender()))
				conflictFreeScore-=100* damageFallOff;
			if(yearLevel.equals(otherSchool.getYearLevel()))
				conflictFreeScore-=100* damageFallOff;
		
		}
	System.out.println("Conflict computation: "+conflictFreeScore );
		return limitScore300(conflictFreeScore);
	}

	  public  float rateLocation(BuildLocation candidateLocation){
		  	
			float overallScore=0;
			float safetyScore=0;
			float accessibilityScore=0;
			float economyScore=0;
			
			safetyScore+=evaluateHighwayDistance(findDistance(candidateLocation, findNearest(candidateLocation, "highway")));
			safetyScore+=evaluateDangerousUtilityDistance(findDistance(candidateLocation, findNearest(candidateLocation, "dangerousUtility")));
			safetyScore+=evaluateBarDistance(findDistance(candidateLocation, findNearest(candidateLocation, "bar")));
			safetyScore+=evaluateFlooding(candidateLocation.getFloodLevel());
			safetyScore+=evaluateFaultLine(candidateLocation.getFaultLineDistance());
			accessibilityScore+=evaluateResidentialDistance(findDistance(candidateLocation, findNearest(candidateLocation, "residential")));
			accessibilityScore+=evaluateConflictOfInterest(candidateLocation);
			economyScore+=evaluatePrice(candidateLocation.getLandPrice());		
			
			
			System.out.println("Safety Score: "+ safetyScore);
			System.out.println("Accessibility Score: "+ accessibilityScore);
			System.out.println("Economy Score: "+ economyScore);
			overallScore+=safetyScore*safetyMultiplier;
			overallScore+=accessibilityScore*accessibilityMultiplier;
			overallScore+=economyScore*economyMultiplier;
			System.out.println("Overall Score: "+ overallScore+ "\n");
			return overallScore;
		  }

	  public  BuildLocation selectTopPicks(int numPicks)
	  {
		  //get data con from DB
		  //FOR EACH ITEM COLLECTED
		  
		  System.out.println("You have called selectTopPicks");
		
	        try {
	        	  Connection conn = DBConnector.connectToDB();
	    		  String query="SELECT * FROM buildlocation";
	    		  Statement stmt = conn.createStatement();
	    		  ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					BuildLocation tempLocation =loadBuildLocation(rs);
					tempLocation.setRating(rateLocation(tempLocation));
					candidateLocationList.add(tempLocation);
					}
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        candidateLocationList.sort((o1, o2) -> o1.getRating().compareTo(o2.getRating()));
	        
	        System.out.println("You have processed "+candidateLocationList.size() +" entries.");
//	        System.out.println("worst:" + candidateLocationList.get(0).getRating());
//	        System.out.println("middle:" + candidateLocationList.get(1).getRating());
//	        System.out.println("best:" + candidateLocationList.get(2).getRating());
	        
	       System.out.println("The best result is" +  candidateLocationList.get(candidateLocationList.size()-1).getFloodLevel()); 
	        
	return candidateLocationList.get(candidateLocationList.size()-1) ;

}
	  
	  
	  public  ArrayList<BuildLocation> selectTopPicksMulti(int numPicks)
	  {
		  //get data con from DB
		  //FOR EACH ITEM COLLECTED
		  
		  System.out.println("You have called selectTopPicks");
		
	        try {
	        	  Connection conn = DBConnector.connectToDB();
	    		  String query="SELECT * FROM buildlocation";
	    		  Statement stmt = conn.createStatement();
	    		  ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					BuildLocation tempLocation =loadBuildLocation(rs);
					tempLocation.setRating(rateLocation(tempLocation));
					candidateLocationList.add(tempLocation);
					}
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        candidateLocationList.sort((o1, o2) -> o1.getRating().compareTo(o2.getRating()));
	        
	        System.out.println("You have processed "+candidateLocationList.size() +" entries.");
//	        System.out.println("worst:" + candidateLocationList.get(0).getRating());
//	        System.out.println("middle:" + candidateLocationList.get(1).getRating());
//	        System.out.println("best:" + candidateLocationList.get(2).getRating());
	        
	       System.out.println("The best result is" +  candidateLocationList.get(candidateLocationList.size()-1).getFloodLevel()); 
	        
	       ArrayList<BuildLocation> topPicks = new ArrayList<BuildLocation>();
	   	for (int i=1;i<=5;i++)
		   	{
		    topPicks.add(candidateLocationList.get(candidateLocationList.size()-i)); 
		   	}
	return topPicks;

}
	  
	  
	  
	  public String generateMetadata(BuildLocation subject)
	  {
		  
		  return "Cheese";
	  }
}
