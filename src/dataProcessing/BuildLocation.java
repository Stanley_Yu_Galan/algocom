package dataProcessing;

public class BuildLocation extends Location{
	
	int id;
	float landPrice;
	float landSize;
	int studentAgePop;
	float educationPercentage;
	//int crimeLevel;
	//int geologicalStability;
	//int accessibility;
	int floodLevel;
	String barangay;
	String faultLineDistance;
	float noiseLevel;
	float rating;
	String metadata ="";

	BuildLocation(int id, double xCoord, double yCoord, float landPrice, float landSize, int studentAgePop, float educationPercentage, int crimeLevel, int geologicalStability, int accessibility, int marketSaturation, float noiseLevel ) {
		    this.id = id;
		    this.xCoord = xCoord;
		    this.yCoord = yCoord;
		    this.landPrice = landPrice;
		    this.landSize = landSize;
		    this.studentAgePop = studentAgePop;
		    this.educationPercentage = educationPercentage;
//		    this.crimeLevel = crimeLevel; // 1-100
//		    this.geologicalStability = geologicalStability; //1-100
//		    this.accessibility = accessibility; //1-100
		    this.noiseLevel = noiseLevel;//
		  }
	BuildLocation()
	{
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setxCoord(double xCoord) {
		this.xCoord = xCoord;
	}

	public void setyCoord(double yCoord) {
		this.yCoord = yCoord;
	}

	public float getLandPrice() {
		return landPrice;
	}

	public void setLandPrice(float landPrice) {
		this.landPrice = landPrice;
	}

	public float getLandSize() {
		return landSize;
	}

	public void setLandSize(float landSize) {
		this.landSize = landSize;
	}

	public int getStudentAgePop() {
		return studentAgePop;
	}

	public void setStudentAgePop(int studentAgePop) {
		this.studentAgePop = studentAgePop;
	}

	public float getEducationPercentage() {
		return educationPercentage;
	}

	public void setEducationPercentage(float educationPercentage) {
		this.educationPercentage = educationPercentage;
	}

//	public int getCrimeLevel() {
//		return crimeLevel;
//	}
//
//	public void setCrimeLevel(int crimeLevel) {
//		this.crimeLevel = crimeLevel;
//	}
//
//	public int getGeologicalStability() {
//		return geologicalStability;
//	}
//
//	public void setGeologicalStability(int geologicalStability) {
//		this.geologicalStability = geologicalStability;
//	}
//
//	public int getAccessibility() {
//		return accessibility;
//	}
//
//	public void setAccessibility(int accessibility) {
//		this.accessibility = accessibility;
//	}

	public float getNoiseLevel() {
		return noiseLevel;
	}

	public void setNoiseLevel(float noiseLevel) {
		this.noiseLevel = noiseLevel;
	}

	public Float getRating() {
		return this.rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}
	public int getFloodLevel() {
		return floodLevel;
	}
	public void setFloodLevel(int floodLevel) {
		this.floodLevel = floodLevel;
	}
	public String getBarangay() {
		return barangay;
	}
	public void setBarangay(String barangay) {
		this.barangay = barangay;
	}
	public String getFaultLineDistance() {
		return faultLineDistance;
	}
	public void setFaultLineDistance(String faultLineDistance) {
		this.faultLineDistance = faultLineDistance;
	}
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
	public void concatMetadata(String metadata) {
		this.metadata += metadata;
	}

		

}
