<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
    <title>School Location Planner</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>


body {
	font-family: "Lato", sans-serif;
	transition: background-color .5s;
}

.form-control
{
    padding-right: 20px;
}

.sidenav {
	height: 100%;
	width: 0;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #111;
	overflow-x: hidden;
	transition: 0.5s;
	padding-top: 60px;
}

.sidenav a {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 25px;
	color: #23e023;
	display: block;
	transition: 0.3s
}

.sidenav a:hover, .offcanvas a:focus {
	color: #23e023;
}

.closebtn {
	position: absolute;
	top: 10px;
	right: 10px;
	font-size: 36px !important;
	margin-left: 10px;
}

#main {
	transition: margin-left .5s;
	padding: 16px;
}

@media screen and (max-height: 450px) {
	.sidenav {
		padding-top: 15px;
	}
	.sidenav a {
		font-size: 18px;
	}
}

html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}

#map {
	height: 100%;
}

greenbg{
	background-color: green;
}

select {
	width: 100px;
}

h1 {
	display: block;
	font-size: 2em;
	margin-top: 0.67em;
	margin-bottom: 0.67em;
	margin-left: 0;
	margin-right: 0;
	font-weight: bold;
}

</style>
    
    
        <meta charset="utf-8">

  
    <script src="jquery-1.11.0.min.js"></script>
    
    </head>


  <body onload="placeConstruction()">


	<div id="mySidenav" class="sidenav form-group">
		<form class="form-inline" method="get" action="dataManager">
	    	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">
				<img src="img/backButton.png" alt="Go to niceone" width="42" height="42" border="0">
			</a>
		
		<h1 align="center" style="color:white;">School Planner</h1>
		<br><br>
		     
		<label for="budget" style="font-size:20px;color:white;padding-left:30px;padding-right:80px;">Budget: </label> 
		
		<input class="form-control input-sm m-r-1" type="number" id="budget" name="budget"> 
		<br><br>
		
		<label for="size" style="font-size:20px;color:white;padding-left:30px;padding-right:20px;">Intended Size: </label>
		<input class="form-control input-sm m-r-1" type="number" id="size" name="size">
		<br><br>
		    
		<label for="gender" style="font-size:20px;color:white;padding-left:30px;padding-right:155px;">Gender: </label>
		<select class="form-control input-sm m-r-1" id="gender" name="gender">
		        
			<option value="male">Male</option>
			<option value="female">Female</option>
			<option value="both">Both</option>
		</select>
		<br><br>
		      
		<label  for="yearLevel" style="font-size:20px;color:white;padding-left:30px;padding-right:92px;">Year Levels: </label>
		<select class="form-control input-sm m-r-1" id="yearLevel" name="yearLevels">
			 <option value="elementary">Elementary</option>
			 <option value="highSchool">High School</option>
			 <option value="both">Both</option>
		</select>    
		<br><br>
		      
		<label for="tuitionFee" style="font-size:20px;color:white;padding-left:30px;padding-right:40px;">Tuition Fee: </label>
		<input type="number" class="form-control input-sm m-r-1" id="tuitionFee" name="tuitionFee">
		<br><br>
			  
		<label for="priority" style="font-size:20px;color:white;padding-left:30px;padding-right:125px;">Priority: </label>
		<select class="form-control input-sm m-r-1" id="priority" name="priority">
			<option value="0">Safety</option>
			<option value="1">Accessibility</option>
			<option value="2">Economy</option>
		</select>
		<br><br>
		
		<label for="capacity" style="font-size:20px;color:white;padding-left:30px;padding-right:60px;">Capacity: </label>
		<input class="form-control input-sm m-r-1" type="number" id="capacity" name="capacity">
		<br><br><br>
		
		<div class="text-center">
			<input type="submit" class="form-control btn btn-success pull-center" id="bt_plan" 
				value="Build Schools">
	    </div>
		
		
		</form>
	</div>
  
  
<div id="main">
  
  <span style="font-size:27px;cursor:pointer" class="glyphicon glyphicon-menu-hamburger" onclick="openNav()"></span>
	<span style="font-size:30px;">NAVIGATOR</span>
</div>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "30%";
    document.getElementById("main").style.marginLeft = "400px";
    document.body.style.backgroundColor = "rgba(192,192,192,1)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "rgba(192,192,192,1)";
}


    function placeConstruction(){

        var image = 'img/construction.png';
        <c:if test="${sessionScope.multiLocation!=null}"> 
        <c:forEach var ="currentLocation" items="${multiLocation}" varStatus="loop">
  		
        
		var coordX = 0;
		var coordY = 0;

			var coordX = ${currentLocation.getxCoord()}
			var coordY = ${currentLocation.getyCoord()}


        var newLoc = new google.maps.LatLng(coordX, coordY);

        var marker = new google.maps.Marker({
            map: map, 
            position: newLoc,
            animation: google.maps.Animation.DROP,
            icon: image
        });
 
        marker.addListener('click', function() {      
            var content ="";
            content += "Ranked #"+${loop.count} +"</br>"
            content += "Coord X: " + ${currentLocation.getxCoord()} +"</br>"
            content += "Coord Y: " + ${currentLocation.getyCoord()} +"</br>"
            content += "Barangay: " + "${currentLocation.getBarangay()}" +"</br>"     
            content += "Land Price per sqm: " + ${currentLocation.getLandPrice()} +"</br>"
            content += "Flood Warning Level: " + ${currentLocation.getFloodLevel()} +"</br>"
            content += "Distance from Fault line: " + "${currentLocation.getFaultLineDistance()}" +"</br>"
            content += "Land Price per sqm: " + ${currentLocation.getLandPrice()} +"</br>"
            content += "Overall Rating: " + ${currentLocation.getRating()} +"/800</br>"
            content += "</br>Distance to other locations: " + "${currentLocation.getMetadata()}" +"</br>" 
	     
             var infowindow = new google.maps.InfoWindow({});
             infowindow.setContent(content);                     
             infowindow.open(map, this);        
             
        });
        </c:forEach>
        </c:if>
    }
</script>


    <div id="map"></div>
    <div id="capture"></div>
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 14.599619, lng: 121.036772},
          zoom: 15
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjysBg08bGGbAFz0A7Q8ASCdp5hTonlNQ&callback=initMap"
    async defer></script>
  </body>
</html>
